import React from "react";


import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";

import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";




const useStyles = makeStyles({
  container: {
    padding: 3,
    width: "100%",
    maxWidth: '30ch',
    backgroundColor: "#424242"
  },
  listItemTextChecked: {
    textDecoration: "line-through",
  },
  listItemTextUnchecked: {
    textDecoration: "none",
  },
});

function TodoList(props) {
  const classes = useStyles();
  const todoList = props.todoList

  return (
    <Container className={classes.container} maxWidth="md">
      {!todoList.length ? (
        <Typography variant="h6" color="error">
          There is nothing to do!!!
        </Typography>
      ) : (
        <List>
          {todoList.map((item) => {
            return (
              <Grid container>
                <ListItem key={item.id}>
                  <Grid item xs={2}>
                    <ListItemIcon>
                      <Checkbox
                        color='secondary'
                        checked={item.status}
                      />
                    </ListItemIcon>
                  </Grid>
                  <Grid item xs={10}>
                    <ListItemText
                      primary={item.value}
                      className={
                        item.status
                          ? classes.listItemTextChecked
                          : classes.listItemTextUnchecked
                      }
                    />
                  </Grid>
                </ListItem>
              </Grid>
            );
          })}
        </List>
      )}
    </Container>
  );
}

export default TodoList;
